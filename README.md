# Mighty Brains


### Instructions to setup the project

#### node version supported <= v14.18.0

1. Please clone the project into local environment
2. Run `npm install`
3. Run `npm run start`
4. Open the http://localhost:4200/ is the customer facing screen (customer)
5. Open the http://localhost:4200/login for service attendant interface. (Store operations).
#### Attendant credentials:-
username:- demouser@gmail.com
password:- 12345


### About

Source code:- 

**Objective:-**
Build a scheduling a pickup with simple user-friendly app to simplify the pickup journey for customer and also for the store operations team.


**Application of the solution:-**
We have built a pluggable interface, that can be integrated into existing mobile apps (Sodimac / Falabella / Tottus ) , allowing customers to schedule their pickups and store team to manage the pickups at store.


**Implementation:- **
1. As part of our implementation, we have mainly two different interfaces.
	1. Customer-facing screens. (Mobile apps)
	2. Service attendant interface (Store team)
2. We have built a PWA app which can be easily integrated like a webview in existing mobile BU apps / can be a standalone application.
3. Service attendant interface to validate pickups and finalize attention.

Customer journey:-

1. Customer will go to the orders sections and further go to specific order section, where customer can select the items that he/she wants to pickup.
2. Then our service, will list the stores that are having the items in inventory and which are nearby for customer to visit.
3. Customer selects the store, date of visit, slot time and confirms the booking.
4. Once confirmed, QR code is generated and customer can take download it / take screenshot of it to present it at store for picking up items. ( QR code act as a way to validate the pickups)

Attendant:- (store team)
1. Attendant logins into the platform.
2. Once the customer presents QR code for pickup, attendant scans QR and check the details of pickup.
3. Delivers the item and mark the pickup as complete.
4. If customer does not visits for the pickup during the time slot, attendant mark the pickup as absent.



**Future plan:-**
1. Make this solution as Software-as--a-service (Saas) platform for integrating external businesses as well in future.
2. Use GPS of customer's mobile to mark that customer as IN_STORE and there by trigger notification to attendant for perparing the pickup.
3. Add more features like (Configurations screen, admin interface for getting reports and analytics of the pickups) etc., into our platform.





