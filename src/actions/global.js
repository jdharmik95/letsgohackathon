export const setUserDetails = (data) => {
  return {
    type: "SET_USER_DETAILS",
    payload: data,
  };
};
export const clearGlobalState = () => {
  return {
    type: "CLEAR_GLOBAL_STATE",
  };
};
