const mockData = {
    "stores": {
        "101": {
            "storeId": "101",
            "name": "Sodimac Homecenter Independencia",
            "timezone": "Asia/kolkata",
            "capacityPerSlot": 10,
            "address": "Av. Pdte. Kennedy 5413, Las Condes, Región Metropolitana",
            "latitude": "-33.424214",
            "longitude": "-70.653809",
            "openDays": "Monday to Friday",
            "time": "9:00 - 18:00",
            "slots": {
                "16_11_2021": {
                    "9_10": {
                        "occupancy": 10
                    },
                    "10_11": {
                        "occupancy": 10
                    },
                    "12_13": {
                        "occupancy": 10
                    },
                    "13_14": {
                        "occupancy": 10
                    },
                    "14_15": {
                        "occupancy": 10
                    },
                    "15_16": {
                        "occupancy": 10
                    }
                },
                "17_11_2021": {
                    "9_10": {
                        "occupancy": 10
                    },
                    "10_11": {
                        "occupancy": 10
                    }
                }
            }
        },
        "102": {
            "storeId": "102",
            "name": "Falabella Homecenter Independencia",
            "timezone": "Asia/kolkata",
            "capacityPerSlot": 10,
            "address": "Av. Pdte. Kennedy 5413, Las Condes, Región Metropolitana",
            "latitude": "-33.424214",
            "longitude": "-70.653809",
            "openDays": "Monday to Friday",
            "time": "9:00 - 18:00",
            "slots": {
                "16_11_2021": {
                    "9_10": {
                        "occupancy": 10
                    },
                    "10_11": {
                        "occupancy": 10
                    },
                    "12_13": {
                        "occupancy": 10
                    },
                    "13_14": {
                        "occupancy": 10
                    },
                    "14_15": {
                        "occupancy": 10
                    },
                    "15_16": {
                        "occupancy": 10
                    }
                },
                "17_11_2021": {
                    "9_10": {
                        "occupancy": 10
                    },
                    "10_11": {
                        "occupancy": 10
                    }
                }
            }
        },
        "103": {
            "storeId": "102",
            "name": "Tottus Homecenter Independencia",
            "timezone": "Asia/kolkata",
            "capacityPerSlot": 10,
            "address": "Av. Pdte. Kennedy 5413, Las Condes, Región Metropolitana",
            "latitude": "-33.424214",
            "longitude": "-70.653809",
            "openDays": "Monday to Friday",
            "time": "9:00 - 18:00",
            "slots": {
                "16_11_2021": {
                    "9_10": {
                        "occupancy": 10
                    },
                    "10_11": {
                        "occupancy": 10
                    },
                    "12_13": {
                        "occupancy": 10
                    },
                    "13_14": {
                        "occupancy": 10
                    },
                    "14_15": {
                        "occupancy": 10
                    },
                    "15_16": {
                        "occupancy": 10
                    }
                },
                "17_11_2021": {
                    "9_10": {
                        "occupancy": 10
                    },
                    "10_11": {
                        "occupancy": 10
                    }
                }
            }
        }
    },
    "bookings": {
        "bookingId": {
            "pickupDate": "16_11_2021",
            "pickupSlot": "10_11",
            "orderId": "1212",
            "customerId": "22121212121212121",
            "items": [
                "PRD_121212",
                "PRD_121215"
            ],
            "status": "BOOKED"
        }
    },
    "products": {
        "PRD_121212": {
            "101": {
                "storeId": "101",
                "inventoryCount": 10,
                "fromDate": "17-11-2021 00:00:00"
            },
            "102": {
                "storeId": "102",
                "inventoryCount": 15,
                "fromDate": "18-11-2021 00:00:00"
            },
            "103": {
                "storeId": "103",
                "inventoryCount": 15,
                "fromDate": "18-11-2021 00:00:00"
            }
        },
        "PRD_121215": {
            "101": {
                "storeId": "101",
                "inventoryCount": 10,
                "fromDate": "17-11-2021 00:00:00"
            },
            "102": {
                "storeId": "102",
                "inventoryCount": 15,
                "fromDate": "18-11-2021 00:00:00"
            }
        },
        "PRD_121216": {
            "101": {
                "storeId": "101",
                "inventoryCount": 10,
                "fromDate": "17-11-2021 00:00:00"
            }
        }
    },
    "customers": {
        "22121212121212121": {
            "firstName": "Pratik",
            "lastName": "Swain",
            "rutId": "121212212",
            "orders": {
                "1212": {
                    "orderId": 1212,
                    "orderDate": "11-11-2021 18:00:00",
                    "status": "ORDERED",
                    "items": {
                        "PRD_121212": {
                            "name": "Desktop",
                            "quantity": 1,
                            "price": 1211
                        },
                        "PRD_121215": {
                            "name": "ipad",
                            "quantity": 1,
                            "price": 1212
                        },
                        "PRD_121216": {
                            "name": "charging cable",
                            "quantity": 1,
                            "price": 1212
                        }
                    }
                },
                "1213": {
                    "orderId": 1212,
                    "orderDate": "11-11-2021 18:00:00",
                    "status": "ORDERED",
                    "items": {
                        "PRD_121212": {
                            "name": "Desktop",
                            "quantity": 1,
                            "price": 1211
                        },
                        "PRD_121215": {
                            "name": "ipad",
                            "quantity": 1,
                            "price": 1212
                        }
                    }
                },
                "1214": {
                    "orderId": 1212,
                    "orderDate": "11-11-2021 18:00:00",
                    "status": "ORDERED",
                    "items": {
                        "PRD_121212": {
                            "name": "Desktop",
                            "quantity": 1,
                            "price": 1211
                        },
                        "PRD_121215": {
                            "name": "ipad",
                            "quantity": 1,
                            "price": 1212
                        }
                    }
                }
            }
        }
    }
}
export default mockData;