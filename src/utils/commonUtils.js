export const splitAndJoin = (data, joinBy='-') => {
    return data?.split('_')?.join(joinBy) || '';
}

