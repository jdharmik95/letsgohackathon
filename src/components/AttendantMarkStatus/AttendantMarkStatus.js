import React, { useEffect, useState } from 'react';
import { Button } from 'react-bootstrap';
import Navbar from '../commonComponent/Navbar/Navbar';
import queryString from 'query-string';
import Skeleton from 'react-loading-skeleton';
import './AttendantMarkStatus.scss';
import { getBooking, getSpecificOrder, updateBookingStatus } from '../../service/Firebase/helpers';
import OrderDisplay from '../commonComponent/OrderDisplay/OrderDisplay';
import { useHistory } from 'react-router';

const AttendantMarkStatus = () => {
    const queryParams = queryString.parse(window.location.search);
    const history = useHistory();
    const [orderDetail, setOrderDetail] = useState();
    const [loader, setLoader] = useState(false);

    useEffect(async () => {
        setLoader(true);
        if (queryParams?.bookingId) {
            const booking = await getBooking(queryParams.bookingId);
            getSpecificOrder(booking.customerId, booking.orderId).then(order => {
                let resultItems = {};
                for (let itemId in order.items) {
                    if (booking.items.includes(itemId)) {
                        resultItems[itemId] = order.items[itemId];
                    }
                }
                order.items = resultItems;
                setOrderDetail(order);
            }).finally(() => {
                setLoader(false);
            });
        }
    }, []);

    const handleUpdateStatus = (status) => {
        if (queryParams?.bookingId) {
            updateBookingStatus(queryParams.bookingId, status).finally(() => history.push('/service-attendant'));
        }
    }

    return (
        <>
            <Navbar />
            <div className="attendant-container">
                <div className="attendant-mark-status-wrap">
                    <div className="attendant-order-details">
                        {!loader ? orderDetail ? <OrderDisplay orderDetail={orderDetail} /> : '' : <Skeleton count={3} height={46} />}
                    </div>
                    <Button className="mark-order-detail-btn" onClick={() => handleUpdateStatus('COMPLETED')} variant="success">Completed</Button>
                    <Button className="mark-order-detail-btn" onClick={() => handleUpdateStatus('ABSENT')}>Absent</Button>
                </div>
            </div>
        </>
    )
}

export default AttendantMarkStatus;