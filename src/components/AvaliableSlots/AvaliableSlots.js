import React from 'react';
import { Button } from 'react-bootstrap';
import './AvaliableSlots.scss';

const AvaliableSlots = (props) => {
    const { slots, handlePickupSlot, capacityPerSlot } = props;
    const avaliableSlots = slots;
    return (
        <>
            {Object.keys(avaliableSlots).length > 0 ?
                <div className="avaliable-slots">
                    <h2 className="avaliable-slots-title">Please select available slots</h2>
                    <div className="slots-box">
                        {Object.keys(avaliableSlots).map((keyName, i) => {
                            return (
                                avaliableSlots[keyName]['occupancy'] >= 0 && avaliableSlots[keyName]['occupancy'] < capacityPerSlot ?
                                    (
                                        <Button className="avaliableslots" key={i} onClick={() => handlePickupSlot(keyName)}>{keyName.replace('_', ' - ')}</Button>
                                    )
                                    
                                    : 
                                        (
                                            <Button className="unavaliableslots" key={i} disabled={true}>{keyName.replace('_', ' - ')}</Button>
                                        )
                            )
                        }
                        )}
                    </div>
                </div>
                : null
            }
        </>
    )
}

export default AvaliableSlots;