import React, { useState , useEffect} from 'react';
import './PickUpDetails.scss';
import { getPickupsForGivenOrder , pickupArrivedAtStore} from '../../service/Firebase/helpers';
import { Button } from 'react-bootstrap';
import Table from 'react-bootstrap/Table'
import { useSelector } from 'react-redux';

const PickUpDetails = ({orderId}) => {

    const [pickUpDetailsToBeShown, setPickUpDetailsToBeShown] = useState([]);
    const [loader, setLoader] = useState(false);
    const [statusAtStore, setStatusAtStore] = useState("");
    const { userId } = useSelector((state) => {
        return {
            userId: state.global.userId,
        }
    });

    useEffect(() => {
        setLoader(true);
        getPickupsForGivenOrder(userId, orderId).then((response) => {
            setPickUpDetailsToBeShown(response)
        }).finally(() => {
            setLoader(false);
        }); 
    }, [])

    const handlePickupChange = async (bookingId) => {
        console.log(bookingId);
        const response = await pickupArrivedAtStore(bookingId);
        console.log(response)
        if (response.status === 'AT_STORE'){
            setStatusAtStore("AT STORE")
        }else{
            setStatusAtStore("")
        }
    }

    return (
        <>
            <div className="detail-wrap">
                <br/>
                <div className="order-head">
                    <div className="order-status">PICKUP DETAILS</div>
                </div>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                        <th>No.</th>
                        <th>DATE</th>
                        <th>TIME SLOT</th>
                        <th>ORDER STATUS</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            pickUpDetailsToBeShown && pickUpDetailsToBeShown.length > 0 ?
                            (   
                                    pickUpDetailsToBeShown?.map((bookedOrders, index) => {
                                        return (
                                            <tr key={index}>
                                                <td>{index+1}</td>
                                                <td>{bookedOrders?.pickupDate.replaceAll('_', '-')}</td>
                                                <td>{bookedOrders?.pickupSlot.replaceAll('_', '-')}</td>
                                                <td>
                                                    {
                                                        bookedOrders?.status === "BOOKED" && statusAtStore!=="CONFIRMED" ? 
                                                        (
                                                        <Button 
                                                            className="picup-buttons"
                                                            onClick = {() => handlePickupChange(bookedOrders?.bookingId)}
                                                        >
                                                            IN STORE
                                                        </Button>
                                                        )
                                                            :  
                                                        (   
                                                            bookedOrders?.status === "BOOKED" && statusAtStore==="CONFIRMED" ?
                                                            <p className="confirmedClass">CONFIRMED</p>
                                                                :
                                                            bookedOrders?.status.replaceAll('_', ' ')
                                                        )
                                                    }
                                                </td>
                                            </tr>
                                        )
                                    })
                            )
                                :
                            (
                                 
                                    <tr>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                
                            )
                        }
                        
                    </tbody>
                </Table>
            </div>
        </>
    )
}

export default PickUpDetails;