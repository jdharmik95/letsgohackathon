import React from 'react';
import { Toast, ToastContainer } from 'react-bootstrap';
import './CustomToast.scss';

const CustomToast = ({ bodyMessage = '', show, onClose, onClick }) => {
    return (
        <>
        <ToastContainer position="top-end" className="custom-toast-container">
            <Toast show={show} delay={3000} autohide onClose={onClose}>
                <Toast.Header>
                    {/* <img src="holder.js/20x20?text=%20" className="rounded me-2" alt="" /> */}
                    <strong className="me-auto">Attentation</strong>
                </Toast.Header>
                <Toast.Body onClick={onClick}>{bodyMessage}</Toast.Body>
            </Toast>
        </ToastContainer>
        </>
    )
}

export default CustomToast;