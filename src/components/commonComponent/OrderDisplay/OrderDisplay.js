import React from 'react';
import './OrderDisplay.scss';

const OrderDisplay = ({orderDetail}) => {
    return (
        <>
            <div className="detail-wrap">
                <div className="order-head">
                    <div className="order-status">{orderDetail?.status || '-'}</div>
                    <div className="order-date">{orderDetail?.orderDate || '-'}</div>
                </div>
                {
                    Object.keys(orderDetail?.items)?.map(itemKey => {
                        const item = orderDetail.items?.[itemKey];
                        return (
                            <div className="item-wrap" key={itemKey}>
                                <div className="item-name">{item?.name || '-'}</div>
                                <div className="item-price">{item?.price || '-'}</div>
                                <div className="item-quantity">{item?.quantity || '-'}</div>
                            </div>
                        )
                    })
                }
            </div>
        </>
    )
}

export default OrderDisplay;