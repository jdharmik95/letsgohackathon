import React from 'react';
import { Modal } from 'react-bootstrap';
import QRScanner from '../../QRScanner/QRScanner';
import crossImage from '../../../images/cross.png';

const QRScannerModal = ({ show, handleClose }) => {
    return (
        <>
            <Modal show={show} onHide={handleClose} centered>
                <Modal.Header>
                    <div className="cross-image" style={{ width: '100%', textAlign: 'right' }}>
                        <img style={{ width: '26px' }} src={crossImage} alt="close" onClick={handleClose} />
                    </div>
                </Modal.Header>
                <Modal.Body>
                    <QRScanner />
                </Modal.Body>
            </Modal>
        </>
    )
}

export default QRScannerModal;