import React from 'react';
import { Navbar as Nav, Container } from 'react-bootstrap';
import './Navbar.scss';

const Navbar = () => {
    return (
        <>
            <Nav bg="primary" variant="dark" className="navbar-custom">
                <Container>
                    <Nav.Brand href="/home">Mighty Brain's</Nav.Brand>
                </Container>
            </Nav>
        </>
    )
}

export default Navbar;