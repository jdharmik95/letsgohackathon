import React, { useRef, useState } from 'react';
import { Form, Button } from 'react-bootstrap';
import reactDom from 'react-dom';
import { authenticateUser, getStore } from '../../service/Firebase/helpers';
import Navbar from '../commonComponent/Navbar/Navbar';
import { useHistory } from 'react-router';
import './Login.scss';
import { setUserDetails } from '../../actions/global';
import { useDispatch } from 'react-redux';

const Login = () => {
    const emailRef = useRef();
    const passwordRef = useRef();
    const history = useHistory();
    const dispatch = useDispatch();
    const [loader, setLoader] = useState(false);

    const handleLogin = (e) => {
        e.preventDefault();   
        const email = reactDom.findDOMNode(emailRef.current);
        const password = reactDom.findDOMNode(passwordRef.current);
        setLoader(true);
        authenticateUser(email?.value, password?.value).then((user) => {
            if (user) {
                getStore(user.storeId).then((store) => {
                    dispatch(setUserDetails({
                        email: user.email,
                        firstName: user.firstName,
                        lastName: user.lastName,
                        storeId: user.storeId,
                        storeName: store.name,
                    }));
                    history.push(`/service-attendant`);
                })
            }
        }).finally(() => setLoader(false));
    }
    return (
        <>
            <Navbar />
            <div className="login-wrapper">
                <Form className="form-wrapper">
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control ref={emailRef} type="email" placeholder="Enter email" />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control ref={passwordRef} type="password" placeholder="Password" />
                    </Form.Group>
                    <div className="login-btn">
                        <Button variant="primary" type="submit" onClick={handleLogin}>
                            Login
                        </Button>
                    </div>
                </Form>
            </div>
        </>
    )
}

export default Login;