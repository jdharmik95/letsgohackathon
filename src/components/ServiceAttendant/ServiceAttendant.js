import React, { useEffect, useRef, useState } from 'react';
import { Button } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { getFirebase } from '../../service/Firebase';
import { getBooking, getMultipleBooking, getStore } from '../../service/Firebase/helpers';
import { splitAndJoin } from '../../utils/commonUtils';
import CustomToast from '../commonComponent/CustomToast/CustomToast';
import Navbar from '../commonComponent/Navbar/Navbar';
import Skeleton from 'react-loading-skeleton'
import QRScannerModal from '../commonComponent/QRScannerModal/QRScannerModal';
import './ServiceAttendant.scss';

const ServiceAttendant = () => {
    const history = useHistory();
    const { serviceName, storeName, storeId } = useSelector(store => {
        return {
            serviceName: store.global.serviceName,
            storeName: store.global.storeName,
            storeId: store.global.storeId,
        }
    });
    const [toggleQR, setToggleQR] = useState(false);
    const [bookingDetails, setBookingDetails] = useState();
    const [message, setMessage] = useState('');
    const [toggleShow, setToggleShow] = useState(false);
    const [loader, setLoader] = useState(false);
    const bookingIdRef = useRef();
    useEffect( async () => {
        setLoader(true);
        if (storeId) {
            getStore(storeId).then(storeDetailRes => {
                getMultipleBooking(storeDetailRes.upcomingPickups).then(bookingDetailsRes => {
                    setBookingDetails(bookingDetailsRes);
                    setLoader(false);
                });
            });
            listenToUpcomingBooking();
        } else history.push('/login');
    }, [storeId]);

    const listenToUpcomingBooking = async () => {
        const URL = `/stores/${storeId}/upcomingPickups`;
        const firebaseInstance = await getFirebase();
        let timeout = null;
        firebaseInstance.database()
            .ref(URL)
            .on('child_added', (snapshot, b) => {
                const bookingId = snapshot.val();
                setToggleShow(false);
                clearTimeout(timeout);
                timeout = setTimeout(() => {
                    getBooking(bookingId).then((bookingDetail) => {
                        bookingIdRef.current = bookingId;
                        setMessage(`Order ${bookingDetail?.orderId || '-'} has arrived at the store`);
                        setToggleShow(true);
                    });
                }, 500);
            });
    }

    return (
        <>
            <CustomToast 
                show={toggleShow} 
                bodyMessage={message} 
                onClose={() => setToggleShow(false)} 
                onClick={() => history.push(`/mark-status?bookingId=${bookingIdRef.current}`)} 
            />
            <Navbar />
            <div className="service-attendant-wrapper">
                <div className="head-container">
                    <div className="head-wrap">
                        <label htmlFor="store-name">Store Name :&ensp;</label>
                        <div id="store-name" className="head-text">{storeName || '-'}</div>
                    </div>
                    <div className="head-wrap">
                        <label htmlFor="service-name">Service Name :&ensp;</label>
                        <div id="service-name" className="head-text">{serviceName || '-'}</div>
                    </div>
                </div>
                <div className="content-wrap">
                    <Button className="scan-button" onClick={() => setToggleQR(!toggleQR)} size='lg'>Scan QR Code</Button>
                    <QRScannerModal show={toggleQR} handleClose={() => setToggleQR(!toggleQR)} />
                    <div className="upcoming-pickups-wrap">
                        <div className="pickups-head">Upcoming Pickups</div>
                        <div className="pickups-list">
                            <div className="booking-details-wrap">
                                <div className="booking-head">ORDER ID</div>
                                <div className="booking-head">SLOT</div>
                                <div className="booking-head">DATE</div>
                            </div>
                            {
                                !loader ? 
                                    bookingDetails ? 
                                        Object.keys(bookingDetails)?.map(bookingkey => {
                                            const bookingDetail = bookingDetails[bookingkey];
                                            return (
                                                <div className="booking-details-wrap" onClick={() => {history.push(`/mark-status?bookingId=${bookingkey}`)}}>
                                                    <div className="booking-order">{bookingDetail.orderId}</div>
                                                    <div className="booking-slot">{splitAndJoin(bookingDetail?.pickupSlot, ' - ')}</div>
                                                    <div className="booking-date">{splitAndJoin(bookingDetail.pickupDate, '/')}</div>
                                                </div>
                                            )
                                        })
                                        : '' 
                                : <Skeleton count={3} height={30} />
                            }
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default ServiceAttendant;