import React, { useEffect, useState } from "react";
import "./style.scss";
import Dropdown from "react-bootstrap/Dropdown"
import { getStoresFromProductIds } from '../../service/Firebase/commonHelper'
import Navbar from '../commonComponent/Navbar/Navbar';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { getOrder, getSlotConfigurations, getStores } from '../../service/Firebase/helpers';
import queryString from 'query-string';
import { useSelector } from 'react-redux';
import AvaliableSlots from '../AvaliableSlots/AvaliableSlots'
import moment from 'moment';
import ConfirmBooking from "../ConfirmBooking/ConfirmBooking";

function PickOrderDetails() {

    const { userId } = useSelector((state) => {
        return {
            userId: state.global.userId,
        }
    });
    const [loader, setLoader] = useState(false);
    const [productsToBeShown, setProductsToBeShown] = useState({});
    const [storesToBeShown, setStoresToBeShown] = useState([]);
    const [checkedItems, setCheckedItems] = useState([]);
    const [startDate, setStartDate] = useState(new Date());
    const [selectedStore, setSelectedStore] = useState('Select Store');
    const [selectedStoreId, setSelectedStoreId] = useState('');
    const [avaliableSlots, setAvaliableSlots] = useState({});
    const [capacityPerSlot, setCapacityPerSlot] = useState(0);
    const [selectedPickupSlot, setSelectedPickupSlot] = useState('');
    const queryParams = queryString.parse(window.location.search);

    useEffect(() => {
        setLoader(true);
        getOrder(userId, queryParams?.orderId).then((response) => {
            const product = response.items
            setProductsToBeShown(product)
        }).finally(() => {
            setLoader(false);
        }); 
    }, [])

    const handleOnChange = async(product) => {
        const itemIndex = checkedItems.indexOf(product);
        setSelectedStore('Select Store');
        setAvaliableSlots({})
        if(itemIndex >= 0) {
            checkedItems.splice(itemIndex, 1);
            setCheckedItems(checkedItems)
        }
        else {
            checkedItems.push(product)
            setCheckedItems(checkedItems)
            }
        const seletedStores = await getStoresFromProductIds(checkedItems);
          getStores(seletedStores).then(response => {
            let storeNames = [];
            response.forEach((stores) => {
                storeNames.push({storeName: stores.name, storeId: stores.storeId })
            });
            setStoresToBeShown(storeNames)
          });
      };

      const selectedStoreOption = (store) => {
          setSelectedStore(store.storeName);
          getAvaliableSlots(store.storeId);
          setSelectedStoreId(store.storeId)
      }

      const getAvaliableSlots = (storeId, selectedDate = startDate) =>{
            getSlotConfigurations(storeId).then((response) => {
                response.forEach((slots) => {
                    const slotDate = slots.date.replaceAll('_','-');
                    const selectedDates = moment(selectedDate).format('DD-MM-YYYY')
                    if(slotDate === selectedDates){
                            setAvaliableSlots({...slots.slots})
                            setCapacityPerSlot(slots.capacityPerSlot)
                    }
                });
            })
      }

      const handleDateChange = (date) =>{
        setAvaliableSlots({})
        setStartDate(date)
        getAvaliableSlots(selectedStoreId,date)
        setSelectedPickupSlot('')
      }

      const handlePickupSlot = (slot) => {
        setSelectedPickupSlot(slot)
      }

    return (
        <>
        <Navbar />  
        <div className="orders-wrapper">
            <h2 className="order-details-title">ORDER DETAILS</h2>
                {Object.keys(productsToBeShown).map((keyName, i) => (
                    <ul className="list-group" key={i}>
                        <li className="list-group-item" key={i}>
                            <input className="form-check-input me-1" type="checkbox" onChange={ () => handleOnChange(keyName)}/> 
                            {productsToBeShown[keyName].name}
                        </li>
                </ul>
                ))}
            <br/>
            {storesToBeShown.length > 0 ?   
            <>
            <h2 className="order-details-title">Please select store</h2>
            <Dropdown className="store-list">
                <Dropdown.Toggle variant="success" id="dropdown-basic" className="store-dropdown-menu">
                   {selectedStore}
                </Dropdown.Toggle>
                <Dropdown.Menu>
                    { storesToBeShown.map((store, i) => (
                        <Dropdown.Item key={i} onClick={() => selectedStoreOption(store)}>{store.storeName}</Dropdown.Item>
                )) }
                </Dropdown.Menu>
            </Dropdown>
            <br/>
            </> : null}
            {selectedStore && selectedStore !== 'Select Store'  ?    
            <>
            <h2 className="order-details-title">Please select date</h2>
            <DatePicker selected={startDate} onChange={(date) => handleDateChange(date)} minDate={new Date()} maxDate={new Date().setDate(new Date().getDate() + 7)}/>
            </>
            : null}
            {selectedStore && Object.keys(avaliableSlots).length > 0 && selectedStore !== 'Select Store' && capacityPerSlot ?
            <AvaliableSlots  slots={avaliableSlots} handlePickupSlot={handlePickupSlot} capacityPerSlot={capacityPerSlot}/>
            : null}
            {
                checkedItems.length > 0 && startDate && selectedPickupSlot && selectedStoreId ? 
            <ConfirmBooking selectedItems={checkedItems} startDate={startDate} selectedPickupSlot={selectedPickupSlot} selectedStoreId={selectedStoreId} buttonEnabled={true}/>
                : <ConfirmBooking selectedItems={checkedItems} startDate={startDate} selectedPickupSlot={selectedPickupSlot} selectedStoreId={selectedStoreId} buttonEnabled={false} />
            }
        </div>
        </>
    )
}

export default PickOrderDetails;
