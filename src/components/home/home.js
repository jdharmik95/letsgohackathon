import React from 'react';
import { useHistory } from 'react-router';
import Navbar from '../commonComponent/Navbar/Navbar';
import { Button } from 'react-bootstrap';
import './home.scss';
import sodimacImage from '../../images/sodimac-homecenter-logo.png';

const Home = () => {
    const history = useHistory();

    return (
        <>
            <Navbar />
            <div className="home-wrapper">
                <div className="home-image-class">
                    <img className="homeImage" src={sodimacImage} alt="close" />
                </div>
                <div className="home-button-wrap">
                    <Button className="home-button" onClick={() => history.push('/my-orders')}>My Orders</Button>
                </div>
            </div>
        </>
    )
}

export default Home;