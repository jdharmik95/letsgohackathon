import React, { useEffect, useState } from 'react';
import { Button } from 'react-bootstrap';
import './ConfirmBooking.scss';
import { makeABooking } from '../../service/Firebase/helpers';
import queryString from 'query-string';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import moment from 'moment';

const ConfirmBooking = (props) => {
    const { selectedItems, startDate, selectedPickupSlot, selectedStoreId, buttonEnabled } = props;
    const checkedItems = selectedItems;
    const pickupDate = moment(startDate).format('DD-MM-YYYY').replaceAll('-', '_');
    const queryParams = queryString.parse(window.location.search);
    const history = useHistory();
    const [loader,setLoader] = useState(false);
    const { userId } = useSelector((state) => {
        return {
            userId: state.global.userId,
        }
    });

    const handleconfirmBooking = async () => {
        const confirmBookingParams = {
            pickupDate: pickupDate,
            pickupSlot: selectedPickupSlot,
            orderId: queryParams?.orderId,
            customerId: userId,
            items: checkedItems,
            storeId: selectedStoreId,
            status: "BOOKED"
        }
        setLoader(true);
        let result = await makeABooking(confirmBookingParams)
        setLoader(false);
        console.log(result);
        if (result) {
            history.push(`/pickup-confirm?bookingId=${result.bookingId}`);
        }
    }

    return (
        <>
            <div className="confirmButton">
                <Button className="confirm-button"
                    variant={buttonEnabled ? "success" : "secondary"}
                    onClick={() => handleconfirmBooking()}
                    disabled={!buttonEnabled}
                >
                    {loader?'Booking ...':'Confirm Pickup' }
                </Button>
            </div>
            <br />
        </>
    )
}

export default ConfirmBooking;