import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import "./style.scss";
import Routes from "../../routes";
import { I18nextProvider } from "react-i18next";
// import "bootstrap";
import i18n from "../../i18n";

function App() {
  return (
    <I18nextProvider i18n={i18n}>
      <Router>
        <Routes />
      </Router>
    </I18nextProvider>
  );
}

export default App;
