import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import queryString from 'query-string';
import Skeleton from 'react-loading-skeleton'
import { getSpecificOrder } from '../../service/Firebase/helpers';
import Navbar from '../commonComponent/Navbar/Navbar';
import { Button } from 'react-bootstrap';
import './OrderDetail.scss';
import OrderDisplay from '../commonComponent/OrderDisplay/OrderDisplay';
import PickUpDetails from '../PickUpDetails/PickUpDetails';

const OrderDetail = () => {
    const queryParams = queryString.parse(window.location.search);
    const history = useHistory();
    const { userId } = useSelector((state) => {
        return {
            userId: state.global.userId,
        }
    });
    const [orderDetail, setOrderDetail] = useState();
    const [loader, setLoader] = useState(false);
    const [orderId, setOrderId] = useState("");

    useEffect(() => {
        setLoader(true);
        if (queryParams?.orderId) {
            setOrderId(queryParams?.orderId);
            getSpecificOrder(userId, queryParams?.orderId).then((res) => {
                setOrderDetail(res);
            }).finally(() => {
                setLoader(false);
            });
        } else {
            history.push('/my-orders');
        }
    }, []);

    const handlePickupOrder = () => {
        history.push(`/order-pickup?orderId=${queryParams?.orderId}`);
    }

    const hasOrder = orderDetail ? Object.keys(orderDetail)?.length : null;
    return (
        <>
            <Navbar />
            <div className="order-detail-container">
                {
                    !loader ?
                        hasOrder ?
                            <>
                                <OrderDisplay orderDetail={orderDetail} />
                                <PickUpDetails orderId={orderId}/>
                                <div className="order-button-wrap">
                                    <Button className="delivery-btn" disabled>Delivery</Button>
                                    <Button className="pickup-btn" onClick={handlePickupOrder}>Pickup</Button>
                                </div>
                            </>
                            : ''
                        : <Skeleton count={3} height={100} />
                }
            </div>
        </>
    )
}

export default OrderDetail;