import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { getOrders } from '../../service/Firebase/helpers';
import Navbar from '../commonComponent/Navbar/Navbar';
import Skeleton from 'react-loading-skeleton'
import './MyOrders.scss';

const MyOrders = () => {
    const history = useHistory();
    const { userId } = useSelector((state) => {
        return {
            userId: state.global.userId,
        }
    });
    const [myOrders, setMyOrders] = useState();
    const [loader, setLoader] = useState(false);

    useEffect(() => {
        setLoader(true);
        if (userId?.length) {
            getOrders(userId).then((orders) => {
                setMyOrders(orders);
            }).finally(() => {
                setLoader(false);
            });
        }
    }, [userId]);

    const handleOrderClick = (orderId) => {
        history.push(`/order-detail?orderId=${orderId}`);
    }

    return (
        <>
            <Navbar />  
            <div className="orders-wrapper-header">
                <div className="order-card-wrap-header">
                                    <div className="order-items-container-header">
                                        <div className="item-wrap-header">
                                            <div className="item-name-header">Item Name</div>
                                            <div className="item-price-header">Item Price</div>
                                            <div className="item-quantity-header">Item Quantity</div>
                                        </div>
                                    </div>
                                </div>
            </div>

            <div className="orders-wrapper-main">
                {
                    !loader ?
                    myOrders ? Object.keys(myOrders)?.map((myOrderkey) => {
                        const myOrder = myOrders[myOrderkey];
                        return (
                                <div className="order-card-wrap-main" onClick={() => handleOrderClick(myOrderkey)}>
                                    <div className="order-status-main">{myOrder.status}</div>
                                    <div className="order-items-container-main">
                                        {
                                            Object.keys(myOrder.items)?.map((itemKey) => {
                                                const item = myOrder.items[itemKey];
                                                return (
                                                    <div className="item-wrap-main">
                                                        <div className="item-name-main">{item.name}</div>
                                                        <div className="item-price-main">{item.price}</div>
                                                        <div className="item-quantity-main">{item.quantity}</div>
                                                    </div>
                                                )
                                            })
                                        }
                                    </div>
                                </div>
                        )
                    })
                    : ''
                    : <Skeleton count={3} height={100} />
                }
            </div>
        </>
    )
}

export default MyOrders;