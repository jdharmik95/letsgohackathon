import React, { useEffect, useState } from 'react';
import "./PickupConfirm.scss";
import { useHistory } from 'react-router';
import QRCode from "react-qr-code";
import { getBooking, getStores } from '../../service/Firebase/helpers';
import queryString from 'query-string';
import Skeleton from 'react-loading-skeleton';
import Navbar from '../commonComponent/Navbar/Navbar';

const PickupConfirm = () => {
    const history = useHistory();
    const { bookingId } = queryString.parse(window.location.search);

    const handleFinish = () => {
        history.push(`/my-orders`);
    }

    const onImageDownload = () => {
        const svg = document.getElementById("QRCode");
        const svgData = new XMLSerializer().serializeToString(svg);
        const canvas = document.createElement("canvas");
        const ctx = canvas.getContext("2d");
        const img = new Image();
        img.onload = () => {
            canvas.width = img.width;
            canvas.height = img.height;
            ctx.drawImage(img, 0, 0);
            const pngFile = canvas.toDataURL("image/png");
            const downloadLink = document.createElement("a");
            downloadLink.download = "QRCode";
            downloadLink.href = `${pngFile}`;
            downloadLink.click();
        };
        img.src = `data:image/svg+xml;base64,${btoa(svgData)}`;
    };

    const [booking, setBooking] = useState();
    const [store, setStore] = useState();
    const [loader, setLoader] = useState(false);

    useEffect(async () => {
        setLoader(true);
        if (bookingId) {
            let booking = await getBooking(bookingId);
            let store = await getStores([booking.storeId]);
            setBooking(booking);
            setStore(store[0]);
            setLoader(false);
        }
    }, [bookingId]);

    return (
        <>
            <Navbar />
            <div className="wrapper">
                {!loader ?
                    store ?
                        <>
                            <div className="store-detail">
                                <h1>{store.name}</h1>,
                                <h2>{store.address}</h2>
                            </div>
                            <div className="qr-code-container">
                                <QRCode id="QRCode" value={bookingId} />
                            </div>
                            <div className="download-button-container">
                                <button className="downloadButton" onClick={() => onImageDownload()}>
                                    Download
                                </button>
                            </div>
                            <div className="instructions">
                                Please show this QR code at store to collect the items on <span className="bold_text">{booking.pickupDate.replaceAll('_', '-')}</span> at selected slot <span className="bold_text">{booking.pickupSlot.replace('_', '-')}</span>
                            </div>
                            <div className="finish-button-container">
                                <button className="finishButton" onClick={() => handleFinish()}>
                                    Done
                                </button>
                            </div>
                        </> : ''
                    : <Skeleton count={1} height={50} />
                }
            </div>
        </>
    );
};

export default PickupConfirm;