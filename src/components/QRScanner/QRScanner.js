import React from 'react';
import "./QRScanner.scss";
import QrReader from 'react-camera-qr';
import { useHistory } from 'react-router';

const QRScanner = () => {
    const history = useHistory();
    const handleScan = data => {
        if (data) {
            history.push(`/mark-status?bookingId=${data}`);
        }
    }
    const handleError = err => {
        alert(err);
    }

    return (
        <div>
            <QrReader
                delay={500}
                onError={handleError}
                onScan={handleScan}
                style={{ width: '100%' }}
            />
        </div>
    )
}

export default QRScanner;