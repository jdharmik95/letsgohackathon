import axios from "axios";
import { store } from "../../index.js";
import { setIsOffline } from "../../actions/global";
import { initializeRefreshToken } from "../../utils/azureLoginCallbacks";
import { initilizeRefetch } from "../../utils/googleLoginCallbacks";
import { getIdToken, getItemFromSession } from "../../utils/SessionStorage";
const config = {
  headers: {
    "Cache-Control": "no-cache",
    "Content-Type": "application/json",
    userrole: "store",
  },
  timeout: 60000,
};

export const apiCancelToken = axios.CancelToken;
export const defaultHeaders = config.headers;
export const API = axios.create(config);

API.interceptors.request.use(
  (config) => {
    store.dispatch(setIsOffline(false));
    const token = getIdToken();
    if (!token) {
      return;
    }
    config.headers["Authorization"] = `Bearer ${token}`;
    return config;
  },
  (err) => Promise.reject(err)
);

export const processApiResponse = (response) => {
  if (response.status === 200) {
    return Promise.resolve(response.data);
  } else {
    return Promise.reject(response.statusText || `Error ${response.status} `);
  }
};

API.interceptors.response.use(processApiResponse, async (error) => {
  if (error.toJSON().message === "Network Error") {
    store.dispatch(setIsOffline(true));
  }
  const statusCode = error.response.status;
  const idp = getItemFromSession("idp");
  const isUnauthorized =
    statusCode === 401 &&
    error.response.data?.errors?.[0].code === "Unauthorized";
  const isInvalidToken =
    statusCode === 401 &&
    error.response.data?.errors?.[0].code === "INVALID_TOKEN";
  if (isUnauthorized) {
    throw new Error(error);
  } else if (isInvalidToken) {
    if (idp === "google") {
      return await initilizeRefetch().then(() => {
        error.config.headers.Authorization = `Bearer ${getIdToken()}`;
        return Promise.resolve(API(error.config));
      });
    } else if (idp === "microsoft_sso") {
      return await initializeRefreshToken().then(() => {
        error.config.headers.Authorization = `Bearer ${getIdToken()}`;
        return Promise.resolve(API(error.config));
      });
    }
  } else {
    return Promise.reject(error);
  }
});
