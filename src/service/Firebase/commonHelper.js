import { getStoreIdsWithGivenProducts } from './helpers'

const getStoresFromProductIds = async (productIds) => {
    const stores = await getStoreIdsWithGivenProducts(productIds)
    return stores;
}

const getSelectedStoreIds = (stores) => {
    const storeIds = []
    for (let i = 0; i < stores.length; i++)
        storeIds.push(stores[i]['stores'])
    if (storeIds.length > 1) {
        const commonStores = storeIds.shift().filter(function (v) {
            return storeIds.every(function (a) {
                return a.indexOf(v) !== -1;
            });
        });
        return commonStores;
    }
    return storeIds[0];
}


export { getSelectedStoreIds, getStoresFromProductIds };