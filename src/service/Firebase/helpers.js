import { v4 as uuidv4 } from 'uuid';
const { getFirebase } = require('./index');
const _ = require('underscore');

const getStores = async (storeIds = []) => {
    const URL = `stores`;
    const firebaseInstance = await getFirebase();
    let promises = storeIds.map((storeId) => {
        return firebaseInstance.database()
            .ref(URL + '/' + storeId)
            .once('value')
            .then(snapshot => {
                if (!snapshot.val()) {
                    return null;
                }
                return snapshot.val();
            });
    });
    let stores = await Promise.all(promises);
    console.log(stores);
    let result = [];
    for (let store of stores) {
        if (store) {
            result.push(store);
        }
    }
    return result;
}
const getOrder = async (customerId, orderId) => {
    console.log("************* get order", customerId, orderId)
    const URL = `customers/${customerId}/orders/${orderId}`;
    const firebaseInstance = await getFirebase();
    return firebaseInstance.database()
        .ref(URL)
        .once('value')
        .then(snapshot => {
            return snapshot.val();
        });
}

const getStore = async (storeId) => {
    const URL = `stores/${storeId}`;
    const firebaseInstance = await getFirebase();
    return firebaseInstance.database()
        .ref(URL)
        .once('value')
        .then(snapshot => {
            return snapshot.val();
        });
}

const getOrders = async (customerId) => {
    const URL = `customers/${customerId}/orders`;
    const firebaseInstance = await getFirebase();
    return firebaseInstance.database()
        .ref(URL)
        .once('value')
        .then(snapshot => {
            return snapshot.val();
        });
}

const getSlotConfigurations = async (storeId) => {

    const URL = `stores/${storeId}`;
    const firebaseInstance = await getFirebase();
    const store = await firebaseInstance.database()
        .ref(URL)
        .once('value')
        .then(snapshot => {
            return snapshot.val();
        });

    if (!store || !store.slots) {
        return [];
    }

    let result = [];
    for (let date in store.slots) {
        result.push({
            date,
            slots: store.slots[date],
            capacityPerSlot: store.capacityPerSlot
        })
    }
    return result;
}

const getBooking = async (bookingId) => {
    const URL = `bookings/${bookingId}`;
    const firebaseInstance = await getFirebase();
    return firebaseInstance.database()
        .ref(URL)
        .once('value')
        .then(snapshot => {
            return { ...snapshot.val(),bookingId };
        });
};

const getStoreIdsWithGivenProducts = async (productIds = []) => {

    // 1. Check inventory of selected products
    // 2. Get stores availability
    const firebaseInstance = await getFirebase();
    let promises = productIds.map((productId) => {
        return firebaseInstance.database()
            .ref('products/' + productId)
            .once('value')
            .then(snapshot => {
                if (!snapshot.val()) {
                    return null;
                }
                return snapshot.val();
            });
    });
    let products = await Promise.all(promises);
    let productStoreIdsMap = {};
    console.log(products);
    for (let i = 0; i < productIds.length; i++) {
        productStoreIdsMap[productIds[i]] = products[i] ? getStoresWithInventory(products[i]) : [];
    }
    console.log('-----');
    return _.intersection(...Object.values(productStoreIdsMap));
}

const getStoresWithInventory = (productStores) => {
    let stores = Object.values(productStores);
    console.log(stores);
    let result = [];
    for (let store of stores) {
        if (store.inventoryCount > 0) {
            result.push(store.storeId);
        }
    }
    return result;
}

const makeABooking = async ({
    pickupDate,
    pickupSlot,
    orderId,
    customerId,
    items,
    status,
    storeId
}) => {

    // 1. get occupancy count and validate if its fine.
    // 2. increment occupancy count by 1
    // 3. create a booking
    const store = await getStore(storeId);
    const order = await getOrder(customerId,orderId);
    console.log('order details are');
    console.log(order);
    const firebaseInstance = await getFirebase();
    const occupancyCountURL = `stores/${storeId}/slots/${pickupDate}/${pickupSlot}/occupancy`;
    // get occupancy count
    let occupancyCount = await firebaseInstance.database()
        .ref(occupancyCountURL)
        .once('value')
        .then(snapshot => {
            return snapshot.val();
        });
    console.log('occupancy count')
    console.log(occupancyCount);
    if (occupancyCount == undefined || occupancyCount >= store.capacityPerSlot) {
        throw new Error('Cannot make booking, occupancy limit reached');
    }
    // increment count
    await firebaseInstance.database()
        .ref(occupancyCountURL)
        .set(occupancyCount + 1);

    let bookingId = uuidv4();
    await firebaseInstance.database()
        .ref(`bookings/${bookingId}`)
        .set({
            pickupDate,
            pickupSlot,
            orderId,
            customerId,
            items,
            status,
            storeId,
            bookingId
        });
    let bookingIds = [bookingId];
    if (order.bookingIds) {
        bookingIds = [...bookingIds,...order.bookingIds]
    }
    await firebaseInstance.database()
        .ref(`customers/${customerId}/orders/${orderId}/bookingIds`)
        .set(bookingIds);
    return {
        pickupDate,
        pickupSlot,
        orderId,
        customerId,
        items,
        status,
        storeId,
        bookingId
    };
};

const pickupArrivedAtStore = async (bookingId) => {
    const URL = `bookings/${bookingId}/status`;
    const firebaseInstance = await getFirebase();
    await firebaseInstance.database()
        .ref(URL)
        .set('AT_STORE');
    const booking = await getBooking(bookingId);
    await addBookingIdInUpcomingTokens(booking.storeId, bookingId);
    return { bookingId, status: 'AT_STORE' };
}

const addBookingIdInUpcomingTokens = async (storeId, bookingId) => {
    const firebaseInstance = await getFirebase();
    let store = await getStore(storeId);
    console.log(store);
    let upcomingPickups = [bookingId];
    if (store.upcomingPickups) {
        upcomingPickups = [...store.upcomingPickups, bookingId];
    }
    console.log('upcoming pickups');
    console.log(upcomingPickups);
    await firebaseInstance.database()
    .ref(`stores/${storeId}/upcomingPickups`)
        .set(upcomingPickups);
}

const updateBookingStatus = async (bookingId, status) => { // status possible values - BOOKED,AT_STORE,COMPLETED,ABSENT
    const URL = `bookings/${bookingId}`;
    const firebaseInstance = await getFirebase();
    await firebaseInstance.database()
    .ref(URL + '/status')
        .set(status);
    
    if (status == 'COMPLETED') {
        let bookingDetail = await firebaseInstance.database()
        .ref(URL)
        .once('value')
        .then(snapshot => {
            return snapshot.val();
        });
        const storeId = bookingDetail.storeId;
        const customerId = bookingDetail.customerId;
        const orderId = bookingDetail.orderId;
        const itemIds = Object.values(bookingDetail.items);
        console.log('item ids are');
        console.log(itemIds);
        let promises = itemIds.map((itemId) => firebaseInstance.database().ref(`customers/${customerId}/orders/${orderId}/items/${itemId}/status`)
                .set('DELIVERED'));
        await Promise.all(promises);
        await removeFromUpComingPickupsInStore(storeId,bookingId);
        let order = await getOrder(customerId, orderId);        
        console.log(order);
        if (isAllItemsInOrderDelivered(Object.values(order.items))) {
            await firebaseInstance.database()
                .ref(`customers/${customerId}/orders/${orderId}/status`)
                .set('DELIVERED');
        }
    }

    return {
        bookingId,
        status
    }

};

const removeFromUpComingPickupsInStore = async (storeId, bookingId) => {
    
    const firebaseInstance = await getFirebase();
    let store = await getStore(storeId);
    console.log(store);
    let upcomingPickups = store.upcomingPickups.filter((pickupId) => {
        if (pickupId == bookingId) {
            return false;
        }
        return true;
    });
    console.log('upcoming pickups');
    console.log(upcomingPickups);
    await firebaseInstance.database()
    .ref(`stores/${storeId}/upcomingPickups`)
        .set(upcomingPickups);
}

const isAllItemsInOrderDelivered = (items) => {
    for (let item of items) {
        if (item.status != 'DELIVERED') {
            return false;
        }
    }
    return true;
}


const getSpecificOrder = async (customerId, orderId) => {
    return getOrders(customerId).then((order) => {
        for (let orderKey of Object.keys(order)) {
            if (orderKey === orderId) return order[orderKey];
        };
    }).finally(() => {
        return {};
    })
}

const getPickupsForGivenOrder = async (customerId, orderId) => {

    const order = await getOrder(customerId, orderId);
    console.log(order);
    const bookingIds = order.bookingIds;
    if (!bookingIds) {
        return [];
    }
    let promises = bookingIds.map((id) => getBooking(id));
    return await Promise.all(promises);
};
const getMultipleBooking = async (bookingIds) => {
    const URL = `bookings`;
    const firebaseInstance = await getFirebase();
    return firebaseInstance.database()
        .ref(URL)
        .once('value')
        .then(snapshot => {
            const bookings = snapshot.val();
            const result = {};
            for (let bookingKey of Object.keys(bookings)) {
                if (bookingIds.includes(bookingKey)) result[bookingKey] = (bookings[bookingKey]);
            }
            return result;
        });
}

const authenticateUser = async (email = '', password = '') => {
    const URL = `users`;
    const firebaseInstance = await getFirebase();
    return firebaseInstance.database()
        .ref(URL)
        .once('value')
        .then(snapshot => {
            const users = snapshot.val();
            for (let userKey of Object.keys(users)) {
                const user = users[userKey];
                if (user.email === email && user.password === password) return users[userKey];
            }
            return false;
        });
}

export {
    updateBookingStatus,
    makeABooking,
    getStoreIdsWithGivenProducts,
    getStores,
    getOrder,
    getOrders,
    getSlotConfigurations,
    getBooking,
    getSpecificOrder,
    getStore,
    pickupArrivedAtStore,
    getPickupsForGivenOrder,
    getMultipleBooking,
    authenticateUser
};