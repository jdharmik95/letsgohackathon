import firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";

const getFirebase = () => {
  const firebaseConfig = {
    authDomain: "letsgohackathon.firebaseapp.com",
    apiKey: "AIzaSyCdc4yiJ_OkzfuZoeNG5ewiXk3cvln1LPE",
    databaseURL: "https://letsgohackathon-default-rtdb.firebaseio.com",
    projectId: "letsgohackathon",
    appId: "1:917217016306:web:838deeb2c340cb3b4e936b",
  };
  if (firebase.apps.length === 0) {
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    firebase
      .auth()
      .signInAnonymously()
      .then((response) => {});
  }
  return firebase;
};

export { getFirebase };
