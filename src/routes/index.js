import React from "react";
import { Switch, Route } from "react-router-dom";
import AttendantMarkStatus from "../components/AttendantMarkStatus/AttendantMarkStatus";
import Home from "../components/home/home";
import MyOrders from "../components/MyOrders/MyOrders";
import PickupConfirm from "../components/PickupConfirm/PickupConfirm";
import NotFound from "../components/NotFound";
import PickOrderDetails from "../components/PickOrderDetails";
import OrderDetail from "../components/OrderDetail/OrderDetail";
import ServiceAttendant from "../components/ServiceAttendant/ServiceAttendant";
import Login from "../components/Login/Login";

export default function AppRoutes() {
  return (
    <Switch>
      <Route exact path={"/home"} component={Home} />
      <Route exact path={"/my-orders"} component={MyOrders} />
      <Route path={"/order-pickup"} component={PickOrderDetails} />
      <Route path="/order-detail" component={OrderDetail} />
      <Route path="/mark-status" component={AttendantMarkStatus} />
      <Route exact path={"/service-attendant"} component={ServiceAttendant} />
      <Route exact path={"/pickup-confirm"} component={PickupConfirm} />
      <Route exact path={"/login"} component={Login} />
      <Route exact path="/" component={Home} />
      <Route exact path="*" component={NotFound} />
    </Switch>
  );
}
