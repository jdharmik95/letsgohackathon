/* eslint-disable no-console */
// import "../polyfills";
// import "@babel/polyfill";
import React, { Suspense } from "react";
import ReactDOM from "react-dom";
import App from "./components/App";
import { createStore } from "redux";
import { Provider } from "react-redux";
import allReducers from "./reducers";
import { Helmet } from "react-helmet";
import 'react-loading-skeleton/dist/skeleton.css'

export const store = createStore(
  allReducers,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
const publicPath = "";
if ("serviceWorker" in navigator) {
  window.addEventListener("load", function () {
    navigator.serviceWorker
      .register(publicPath + "worker.js")
      .then(
        function (registration) {
          console.log("Worker registration successful", registration.scope);
        },
        function (err) {
          console.log("Worker registration failed", err);
        }
      )
      .catch(function (err) {
        console.log(err);
      });
  });
} else {
  console.log("Service Worker is not supported by browser.");
}
ReactDOM.render(
  <Provider store={store}>
    <Helmet>
      <link rel="apple-touch-icon" href={`${publicPath}logo.png`} />
      <link rel="manifest" href={`${publicPath}manifest.json`} />
    </Helmet>
    <React.StrictMode>
      <Suspense fallback={null}>
        <App />
      </Suspense>
    </React.StrictMode>
  </Provider>,
  document.getElementById("root")
);
