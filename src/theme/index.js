export const setTheme = (bu) => {
  document.documentElement.className = "";
  if (bu) {
    document.documentElement.classList.add(`theme-${bu}`);
  } else {
    document.documentElement.classList.add(`theme-default`);
  }
};
