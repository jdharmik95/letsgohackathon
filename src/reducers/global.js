const initialState = {
  isOffline: false,
  userId: '22121212121212121',
  bookingId: '4565654645656345345435345',
  serviceName: 'pickup',
  // storeName: 'Sodimac Homecenter Independencia',
  // storeId: '100',
};
const globalReducer = (state = initialState, action) => {
  switch (action.type) {
    case "SET_USER_DETAILS":
      return {
        ...state,
        ...action.payload,
      };
    case "CLEAR_GLOBAL_STATE":
      return {
        ...initialState,
      };
    default:
      return state;
  }
};
export default globalReducer;
