import globalReducer from "./global";
import { combineReducers } from "redux";
const allReducers = combineReducers({
  global: globalReducer,
});
export default allReducers;
